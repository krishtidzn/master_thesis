var handlebars = require('handlebars');
//var template = handlebars.compile(source);
var fs = require('fs');
var jsonObj = require("./simpler_object_for_handlebars.json");
var moment = require('moment');

/*var request = require('request');
request('http://localhost:8080/opencds-decision-support-service/evaluate', function (error, response, body) {
  

  }
})*/

var soap = require('soap');

var ws = require('ws.js');
var Http = ws.Http;
var Security = ws.Security;
// var UsernameToken = ws.UsernameToken;
var libxmljs = require("libxmljs");
var parser = require('xml2json');

 var xpath = require('xpath')
      , dom = require('xmldom').DOMParser

var re = /<base64EncodedPayload>(.*?)<\/base64EncodedPayload>/;


// var url = 'http://localhost:8080/opencds-decision-support-service/evaluate?wsdl';

var request = '<SOAP-ENV:Envelope  xmlns:SOAP-ENV="http://www.w3.org/2003/05/soap-envelope"  xmlns:dss="http://www.omg.org/spec/CDSS/201105/dss">'+
			  '<SOAP-ENV:Header/>' +
			  '<SOAP-ENV:Body>'+
				  '<dss:evaluateAtSpecifiedTime>' +
				  '<interactionId scopingEntityId="edu.utah" interactionId="123456" submissionTime="2012-01-11T00:00:00.000"/>'+
				  '<specifiedTime>2011-01-01T00:00:00.000</specifiedTime>'+
				  '<evaluationRequest>'+
			        '<kmEvaluationRequest>' +
			          '<kmId scopingEntityId="org.opencds" businessId="NQF_0032_v1" version="1.1.0"/>' +
			        '</kmEvaluationRequest>' +
			        '<dataRequirementItemData>' +
			          '<driId itemId="payload001">' +
			            '<containingEntityId scopingEntityId="edu.utah" businessId="123.456.7.8.2.1" version="1.0.0"/>' +
			          '</driId>' +
			          '<data>' +
			            '<informationModelSSId scopingEntityId="org.opencds.vmr" businessId="VMR" version="1.0"/>' +
			          	'<base64EncodedPayload>PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxpbjpjZHNJbnB1dCB4bWxuczppbj0ib3JnLm9wZW5jZHMudm1yLnYxXzAuc2NoZW1hLmNkc2lucHV0Ig0KCQl4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiANCgkJeG1sbnM6ZHQ9Im9yZy5vcGVuY2RzLnZtci52MV8wLnNjaGVtYS5kYXRhdHlwZXMiDQoJCXhzaTpzY2hlbWFMb2NhdGlvbj0ib3JnLm9wZW5jZHMudm1yLnYxXzAuc2NoZW1hLmNkc2lucHV0IC4uXHNjaGVtYVxjZHNJbnB1dC54c2QiPg0KCTx2bXJJbnB1dD4NCgkJPHRlbXBsYXRlSWQgcm9vdD0iMi4xNi44NDAuMS4xMTM4ODMuMy43OTUuMTEuMS4xIi8+DQoJCTxwYXRpZW50Pg0KCQkJPGlkIHJvb3Q9IjIuMTYuODQwLjEuMTEzODgzLjE5LjUiIGV4dGVuc2lvbj0iOTk2LTc1Ni00OTUiLz4NCgkJCTwhLS1hbHRlcm5hdGUgZm9ybSBvZiBpZDogIDxpZCByb290PSJhR1VJRCIvPiAgLS0+DQoJCQk8ZGVtb2dyYXBoaWNzPg0KCQkJCTxiaXJ0aFRpbWUgdmFsdWU9IjE5NjkwNjI4Ii8+IDwhLS0gNDIuNSB5byBhdCAxMi8zMS8yMDExIC0tPg0KCQkJCTxnZW5kZXIgY29kZT0iMTAxNzQiIGNvZGVTeXN0ZW09IjIuMTYuODQwLjEuMTEzODgzLjUiIGRpc3BsYXlOYW1lPSJGZW1hbGUiLz4NCgkJCTwvZGVtb2dyYXBoaWNzPg0KCQkJPGNsaW5pY2FsU3RhdGVtZW50cz4NCgkJCQk8ZW5jb3VudGVyRXZlbnRzPg0KCQkJCQk8ZW5jb3VudGVyRXZlbnQ+DQoJCQkJCQk8dGVtcGxhdGVJZCByb290PSIyLjE2Ljg0MC4xLjExMzg4My4zLjc5NSIvPg0KCQkJCQkJPGlkIHJvb3Q9IjEuMi4zIiBleHRlbnNpb249IkVuYzAwMSIvPg0KCQkJCQkJPGRhdGFTb3VyY2VUeXBlIGNvZGU9IkNsaW5pY2FsIiBjb2RlU3lzdGVtPSIyLjE2Ljg0MC4xLjExMzg4My4zLjc5NSIvPg0KCQkJCQkJPGVuY291bnRlclR5cGUgY29kZT0iOTkyMDEiIGNvZGVTeXN0ZW09IjIuMTYuODQwLjEuMTEzODgzLjYuMTIiIGRpc3BsYXlOYW1lPSJPdXRwYXRpZW50IGVuY291bnRlciIvPg0KCQkJCQkJPGVuY291bnRlckV2ZW50VGltZSBsb3c9IjIwMTEwMTAxIiBoaWdoPSIyMDExMDEwMSIvPg0KCQkJCQk8L2VuY291bnRlckV2ZW50Pg0KCQkJCTwvZW5jb3VudGVyRXZlbnRzPg0KCQkJCTxvYnNlcnZhdGlvblJlc3VsdHM+DQoJCQkJCTxvYnNlcnZhdGlvblJlc3VsdD4NCgkJCQkJCTx0ZW1wbGF0ZUlkIHJvb3Q9IjIuMTYuODQwLjEuMTEzODgzLjMuNzk1Ii8+DQoJCQkJCQk8aWQgcm9vdD0iMS4yLjMiIGV4dGVuc2lvbj0iT2JzMDAxIi8+DQoJCQkJCQk8ZGF0YVNvdXJjZVR5cGUgY29kZT0iQ2xpbmljYWwiIGNvZGVTeXN0ZW09IjIuMTYuODQwLjEuMTEzODgzLjMuNzk1Ii8+DQoJCQkJCQk8b2JzZXJ2YXRpb25Gb2N1cyBjb2RlPSI4ODE0NyIgY29kZVN5c3RlbT0iMi4xNi44NDAuMS4xMTM4ODMuNi4xMiIgZGlzcGxheU5hbWU9IkNlcnZpY2FsIG9yIHZhZ2luYWwgY3l0b3BhdGhvbG9neSBzbWVhciIvPg0KCQkJCQkJPG9ic2VydmF0aW9uRXZlbnRUaW1lIGxvdz0iMjAxMDA2MDEiIGhpZ2g9IjIwMTAwNjAxIi8+DQoJCQkJCTwvb2JzZXJ2YXRpb25SZXN1bHQ+DQoJCQkJPC9vYnNlcnZhdGlvblJlc3VsdHM+CQkJDQoJCQk8L2NsaW5pY2FsU3RhdGVtZW50cz4NCgkJPC9wYXRpZW50PgkNCgk8L3ZtcklucHV0Pg0KPC9pbjpjZHNJbnB1dD4NCg==</base64EncodedPayload>'+
			          '</data>'+
			        '</dataRequirementItemData>' +
			      '</evaluationRequest>' +
			    '</dss:evaluateAtSpecifiedTime>' +
			  '</SOAP-ENV:Body>' +
			'</SOAP-ENV:Envelope>'

var args =  { request: request 
           , url: "http://localhost:8080/opencds-decision-support-service/evaluate"
           , action: "Describe"
           , contentType: "text/xml" 
           }


var handlers =  [ new Http() ]
ws.send(handlers, args, function(args) { 
     var resp = args.response;
     var matches=resp.match(re);
     var stringXml = new Buffer (matches[1], 'base64').toString('utf8');
     // console.log(stringXml); 

 //     var xml='<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'+
 //     '<ns3:cdsOutput xmlns:ns2="org.opencds.vmr.v1_0.schema.vmr" xmlns:ns3="org.opencds.vmr.v1_0.schema.cdsoutput" xmlns:ns4="org.opencds.vmr.v1_0.schema.cdsinput" xmlns:ns5="org.opencds.vmr.v1_0.schema.cdsinput.specification">'+
 //    	'<vmrOutput>'+
	//         '<templateId root="2.16.840.1.113883.3.795.11.1.1" extension=""/>'+
	//         '<patient>'+
	//             '<id root="2.16.840.1.113883.19.5" extension="996-756-495"/>'+
	//             '<demographics>'+
	// 	                '<birthTime value="19690628000000.000+0200"/>'+
	// 	                '<gender code="10174" codeSystem="2.16.840.1.113883.5" displayName="Female"/>'+
 //            	'</demographics>'+
 //            	'<clinicalStatements>'+
 //                '<observationResults>'+
	//                     '<observationResult>'+
	//                         '<id root="ROOT"/>'+
	//                         '<observationFocus code="C53" codeSystem="2.16.840.1.113883.3.795.12.1.1" codeSystemName="OpenCDS concepts" displayName="Quality measure"/>'+
	//                         '<observationEventTime high="20110101000000.000+0200" low="20110101000000.000+0200"/>'+
	//                         '<observationValue/>'+
	//                         '<relatedClinicalStatement>'+
	//                             '<targetRelationshipToSource code="C67" codeSystem="2.16.840.1.113883.3.795.12.1.1" codeSystemName="OpenCDS concepts" displayName="Is contained by"/>'+
	//                      '<observationResult>'+
	// 				'<id root="Numerator"/>'+
	// 				'<observationFocus code="C55" codeSystem="2.16.840.1.113883.3.795.12.1.1" codeSystemName="OpenCDS concepts" displayName="Num and denom criteria met"/>'+
	// 				'<observationEventTime high="20110101000000.000+0200" low="20110101000000.000+0200"/>'+
	// 				'<observationValue>'+
	// 				    '<boolean value="false"/>'+
	// 				'</observationValue>'+
	//                             '</observationResult>'+
	//                         '</relatedClinicalStatement>'+
	//                         '<relatedClinicalStatement>'+
	//                             '<targetRelationshipToSource code="C67" codeSystem="2.16.840.1.113883.3.795.12.1.1" codeSystemName="OpenCDS concepts" displayName="Is contained by"/>'+
	// 									
	// '<observationResult>'+
	// 					'<id root="Denominator"/>'+
	// 					'<observationFocus code="C54" codeSystem="2.16.840.1.113883.3.795.12.1.1" codeSystemName="OpenCDS concepts" displayName="Denominator criteria met"/>'+
	// 					'<observationEventTime high="20110101000000.000+0200" low="20110101000000.000+0200"/>'+
	// 					'<observationValue>'+
	// 						'<boolean value="false"/>'+
	// 					'</observationValue>'+
	//                             '</observationResult>'+
	//                         '</relatedClinicalStatement>'+
	//                     '</observationResult>'+
	//                 '</observationResults>'+
	//             '</clinicalStatements>'+
 //        	'</patient>'+
 //    	'</vmrOutput>'+
	// '</ns3:cdsOutput>';



     // var xmlDoc = libxmljs.parseXml(xml);

     var doc = new dom().parseFromString(stringXml);

     var num = xpath.select("//observationResult[2]/id/@root", doc)[0].nodeValue;
     console.log(num);

     var numValue = xpath.select("//observationResult[2]/observationValue/boolean/@value", doc)[0].nodeValue;
     console.log(numValue);


     var denom = xpath.select("//observationResult[3]/id/@root", doc)[0].nodeValue;
     console.log(denom);

     var denomValue = xpath.select("//observationResult[3]/observationValue/boolean/@value", doc)[0].nodeValue;
     // console.log(denomValue);

    
    var jsonObj = {num:numValue,
    				denom:denomValue};
	console.log(JSON.stringify(jsonObj));

	fs.writeFile("output.txt", JSON.stringify(jsonObj), function(err) {
    if(err) {
        return console.log(err);
    }

    console.log("The file was saved!");
	}); 
 //    var text = '{ "quality_measure" : [' +
	// '{ "num":"John" },' +
	// '{ "firstName":"Peter" } ]}';

	// var obj = JSON.parse(text);

	// console.log(obj);
     // console.log(nodes[0].localName + ": " + nodes[0].firstChild.data)
     // console.log("node: " + nodes[0].toString())

     // console.log(JSON.stringify(xmlDoc.errors())); 
    
     // console.log(JSON.stringify(xmlDoc));
    

     // var jsonObj = JSON.parse(stringXml);
     // console.log(jsonObj);

     // var elm = xmlDoc.get('//text()');

     // var children = xmlDoc.root();
     // var child=children[0];
     

     // var gchild = xmlDoc.get('//clinicalStatements/*');
     // console.log(gchild);

     // console.log(child.attr('root').value()); 

     // var json = parser.toJson(xmlDoc);

     // console.log(json);

     // var doc = JSON.stringify(xmlDoc);

     // console.log(JSON.stringify(xmlDoc));

     	// console.log(doc.errors(JSON.stringify(xmlDoc)));
     
     // console.log(xmlDoc.errors());

})


 // console.log(new Buffer("SGVsbG8gV29ybGQ=", 'base64').toString('ascii'))

// var matches=resp.match(re);

// console.log(matches);

// var res = args.payloadData = new Buffer(payload).toString('base64');
// console.log(res);






// var b = new Buffer('PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4NCjxpbjpjZHNJbnB1dCB4bWxuczppbj0ib3JnLm9wZW5jZHM'+
// 			'udm1yLnYxXzAuc2NoZW1hLmNkc2lucHV0Ig0KCQl4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiANC'+
// 			'gkJeG1sbnM6ZHQ9Im9yZy5vcGVuY2RzLnZtci52'+
// 			'MV8wLnNjaGVtYS5kYXRhdHlwZXMiDQoJCXhzaTpzY2hlbWFMb2NhdGlvbj0ib3JnLm9wZW5jZHMudm1yLnYxXzAuc2NoZW1hLmNkc2lucHV' +
// 			'0IC4uXHNjaGVtYVxjZHNJbnB1dC54c2QiPg0KCTx2bXJJbnB1dD4NCgkJPHRlbXBsYXRlSWQgcm9vdD0iMi4xNi44NDAuMS4xMTM4ODMuMy43' +
// 			'OTUuMTEuMS4xIi8+DQoJCTxwYXRpZW50Pg0KCQkJPGlkIHJvb3Q9IjIuMTYuODQwLjEuMTEzODgzLjE5LjUiIGV4dGVuc2lvbj0iOTk2LTc1Ni' +
// 			'00OTUiLz4NCgkJCTwhLS1hbHRlcm5hdGUgZm9ybSBvZiBpZDogIDxpZCByb290PSJhR1VJRCIvPiAgLS0+DQoJCQk8ZGVtb2dyYXBoaWNzPg0KCQkJCTxiaXJ0aFRpbWUgd' +
// 			'mFsdWU9IjE5NjkwNjI4Ii8+IDwhLS0gNDIuNSB5byBhdCAxMi8zMS8yMDExIC0tPg0KCQkJCTxnZW5kZXIgY29kZT0iMTAxNzQiIGNvZGVTeXN0ZW09IjIuMTYuODQwLjEuMTEzODgzL' +
// 			'jUiIGRpc3BsYXlOYW1lPSJGZW1hbGUiLz4NCgkJCTwvZGVtb2dyYXBoaWNzPg0KCQkJPGNsaW5pY2FsU3RhdGVtZW50cz4NCgkJCQk8ZW5jb3VudGVyRXZlbnRzPg0KCQkJCQk8ZW5jb3VudGVyRXZlbnQ+DQoJCQkJCQk8dGVtcGxhdGVJZCByb290PSIyLjE2Ljg0MC4xLjExMzg4My4zLjc5NSIvPg0KCQkJCQkJPGlkIHJvb3Q9IjEuMi4zIiBleHRlbnNpb249IkVuYzAwMSIvPg0KCQkJCQkJPGRhdGFTb3VyY2VUeXBlIGNvZGU9IkNsaW5pY2FsIiBjb2RlU3lzdGVtPSIyLjE2Ljg0MC4xLjExMzg4My4zLjc5NSIvPg0KCQkJCQkJPGVuY291bnRlclR5cGUgY29kZT0iOTkyMDEiIGNvZGVTeXN0ZW09IjIuMTYuODQwLjEuMTEzODgzLjYuMTIiIGRpc3BsYXlOYW1lPSJPdXRwYXRpZW50IGVuY291bnRlciIvPg0KCQkJCQkJPGVuY291bnRlckV2ZW50VGltZSBsb3c9IjIwMTEwMTAxIiBoaWdoPSIyMDExMDEwMSIvPg0KCQkJCQk8L2VuY291bnRlckV2ZW50Pg0KCQkJCTwvZW5jb3VudGVyRXZlbnRzPg0KCQkJCTxvYnNlcnZhdGlvblJlc3VsdHM+DQoJCQkJCTxvYnNlcnZhdGlvblJlc3VsdD4NCgkJCQkJCTx0ZW1wbGF0ZUlkIHJvb3Q9IjIuMTYuODQwLjEuMTEzODgzLjMuNzk1Ii8+DQoJCQkJCQk8aWQgcm9vdD0iMS4yLjMiIGV4dGVuc2lvbj0iT2JzMDAxIi8+DQoJCQkJCQk8ZGF0YVNvdXJjZVR5cGUgY29kZT0iQ2xpbmljYWwiIGNvZGVTeXN0ZW09IjIuMTYuODQwLjEuMTEzODgzLjMuNzk1Ii8+DQoJCQkJCQk8b2JzZXJ2YXRpb25Gb2N1cyBjb2RlPSI4ODE0NyIgY29kZVN5c3RlbT0iMi4xNi44NDAuMS4xMTM4ODMuNi4xMiIgZGlzcGxheU5hbWU9IkNlcnZpY2FsIG9yIHZhZ2luYWwgY3l0b3BhdGhvbG9neSBzbWVhciIvPg0KCQkJCQkJPG9ic2Vyd' +
// 			'mF0aW9uRXZlbnRUaW1lIGxvdz0iMjAxMDA2MDEiIGhpZ2g9IjIwMTAwNjAxIi8+DQoJCQkJCTwvb2JzZXJ2YXRpb25SZXN1bHQ++DQoJCQkJPC9vYnNlcnZhdGlvblJlc3VsdHM+CQkJDQ' +
// 			'oJCQk8L2NsaW5pY2FsU3RhdGVtZW50cz4NCgkJPC9wYXRpZW50PgkNCgk8L3ZtcklucHV0Pg0KPC9pbjpjZHNJbnB1dD4NCg==');
// var s = b.toString('base64');

// console.log(s);

handlebars.registerHelper("formatDate", function (textDate) {
  if(textDate)
  return moment(textDate).format('YYYYMMDD');
});



fs.readFile('NQF_31.template', {
	'encoding': 'utf8'
}, function(err, source) {
	if (err) throw err;
	//console.log(source);
	var template = handlebars.compile(source);
	var vmrOutput = template(jsonObj);
	// console.log(vmrOutput);
});