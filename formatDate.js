var moment = require('moment');

Handlebars.registerHelper("formatDate", function(‘formatDateTimeHL7’, formatDateTimeHL7) {

   function formatDateHL7(textDate) {
	   if(textDate)
		   return moment(textDate).format('YYYYMMDDHHmmssZZ');
   }
});


